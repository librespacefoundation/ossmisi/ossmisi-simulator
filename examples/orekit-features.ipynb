{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A certain number of features in `ephemerista` are based on the [Orekit library](https://www.orekit.org/), and in particular on its [Jpype Python wrapper](https://pypi.org/project/orekit-jpype/) which was recently made available on PyPi.\n",
    "Though available in Python, the Orekit Python wrapper interacts with a Java virtual machine in the background, therefore `ephemerista` also depends on a JDK, which is usually provided by [jdk4py](https://pypi.org/project/jdk4py/), available as a dependency of `ephemerista`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Before starting"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A CSV file containing Earth Orientation Parameters (EOP) must be passed to `ephemerista`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import ephemerista\n",
    "\n",
    "ephemerista.init(eop_path=\"../tests/resources/finals2000A.all.csv\", spk_path=\"../tests/resources/de440s.bsp\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Propagation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`ephemerista`'s `NumericalPropagator` and `SemiAnalyticalPropagator` inherit from the same base class, `OrekitPropagator`, because most of the code is similar between both. They are based on their Orekit counterparts, [NumericalPropagator](https://www.orekit.org/static/apidocs/org/orekit/propagation/numerical/NumericalPropagator.html) and [DSSTPropagator](https://www.orekit.org/static/apidocs/org/orekit/propagation/semianalytical/dsst/DSSTPropagator.html):\n",
    "\n",
    "* The `NumericalPropagator` is a full-fledged numerical propagator based on a [Dormand-Prince 8(5,3) variable step integrator](https://www.hipparchus.org/apidocs/org/hipparchus/ode/nonstiff/DormandPrince853Integrator.html), featuring most perturbation force models:\n",
    "  * [Holmes-Featherstone gravitational model](https://www.orekit.org/static/apidocs/org/orekit/forces/gravity/HolmesFeatherstoneAttractionModel.html), up to an arbitrary degree and order.\n",
    "  * [Third-body attraction](https://www.orekit.org/static/apidocs/org/orekit/forces/gravity/ThirdBodyAttraction.html) from any planet of the solar system, the Sun or the Moon\n",
    "  * [Solar radiation pressure](https://www.orekit.org/static/apidocs/org/orekit/forces/radiation/SolarRadiationPressure.html), for now isotropic\n",
    "  * Atmospheric drag based on the [NRLMSISE00 density model](https://www.orekit.org/static/apidocs/org/orekit/models/earth/atmosphere/NRLMSISE00.html), using three-hourly [CSSI space weather data](https://www.orekit.org/static/apidocs/org/orekit/models/earth/atmosphere/data/CssiSpaceWeatherData.html). For now isotropic.\n",
    "\n",
    "* The `SemiAnalyticalPropagator` uses the Orekit implementation of the Draper Semi-analytical Satellite Theory (DSST), which describes a semianalytical propagator that combines the accuracy of numerical propagators with the speed of analytical propagators. It features similar force models to the `NumericalPropagator`, and the Python parameters are the same."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Initialization"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The propagator's initial state vector must be provided as a `TwoBody` object, either in `Keplerian` or in `Cartesian` representation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### From `Cartesian` (position, velocity)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "from ephemerista.coords.twobody import Cartesian\n",
    "from ephemerista.time import Time\n",
    "\n",
    "time_start = Time.from_iso(\"TDB\", \"2016-05-30T12:00:00\")\n",
    "\n",
    "r = np.array([6068.27927, -1692.84394, -2516.61918])  # km\n",
    "v = np.array([-0.660415582, 5.495938726, -5.303093233])  # km/s\n",
    "\n",
    "state_init = Cartesian.from_rv(time_start, r, v)\n",
    "display(state_init)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### From `Keplerian`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ephemerista.angles import Angle\n",
    "from ephemerista.coords.anomalies import TrueAnomaly\n",
    "from ephemerista.coords.shapes import RadiiShape\n",
    "from ephemerista.coords.twobody import Inclination, Keplerian\n",
    "from ephemerista.time import Time\n",
    "\n",
    "time_start = Time.from_iso(\"TDB\", \"2016-05-30T12:00:00\")\n",
    "\n",
    "state_init = Keplerian(\n",
    "    time=time_start,\n",
    "    shape=RadiiShape(ra=10000.0, rp=6800.0),  # km\n",
    "    inc=Inclination(degrees=98.0),\n",
    "    node=Angle(degrees=0.0),\n",
    "    arg=Angle(degrees=0.0),\n",
    "    anomaly=TrueAnomaly(degrees=90.0),\n",
    ")\n",
    "display(state_init)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Declaring and configuring a propagator"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The propagator takes at least the initial state as parameter. The initial state can later be overriden though by calling the method `set_initial_state`.\n",
    "\n",
    "Declaring a simple propagator with default parameters, in either the numerical or the semi-analytical flavour:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ephemerista.propagators.orekit.numerical import NumericalPropagator\n",
    "\n",
    "propagator = NumericalPropagator(state_init=state_init)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ephemerista.propagators.orekit.semianalytical import SemiAnalyticalPropagator\n",
    "\n",
    "propagator = SemiAnalyticalPropagator(state_init=state_init)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The constructor internally starts the JVM for Orekit java, set ups some physical data (e.g. JPL DE440 kernels, etc.), and configures the force models."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## General propagator configuration"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following `float` arguments can be passed to the `NumericalPropagator` or `SemiAnalyticalPropagator` constructors for general configuration of the propagators:\n",
    "\n",
    "* `prop_min_step`: Minimum integrator step, default is 1 millisecond. [Reference](https://www.hipparchus.org/apidocs/org/hipparchus/ode/nonstiff/DormandPrince853Integrator.html#%3Cinit%3E(double,double,double%5B%5D,double%5B%5D))\n",
    "* `prop_max_step`: Maximum integrator step, default is 3600 seconds. [Reference](https://www.hipparchus.org/apidocs/org/hipparchus/ode/nonstiff/DormandPrince853Integrator.html#%3Cinit%3E(double,double,double%5B%5D,double%5B%5D))\n",
    "* `prop_init_step` Initial integrator step, default is 60 seconds. [Reference](https://www.hipparchus.org/apidocs/org/hipparchus/ode/nonstiff/DormandPrince853Integrator.html#%3Cinit%3E(double,double,double%5B%5D,double%5B%5D))\n",
    "* `prop_position_error`: Order of magnitude of the desired position error of the integrator for a sample LEO orbit, default is 10 meters. Decreasing this value can improve the propagation accuracy for complex force models, but will slow down the propagator. [Reference](https://www.orekit.org/static/apidocs/org/orekit/propagation/numerical/NumericalPropagator.html#tolerances(double,org.orekit.orbits.Orbit,org.orekit.orbits.OrbitType))\n",
    "\n",
    "* `mass`: spacecraft mass in kilograms, default is 1000 kilograms."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Force model configuration"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Without any arguments passed to the constructor, the default force model uses Earth spherical harmonics up to degree and order 4. As both the `NumericalPropagator` and `SemiAnalyticalPropagator` use the same arguments, in the following we will show only examples with the `NumericalPropagator`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Loading a custom gravity model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Only the [4 file formats supported by Orekit](https://www.orekit.org/static/apidocs/org/orekit/forces/gravity/potential/PotentialCoefficientsReader.html) are available in `ephemerista`. For instance to load a ICGEM file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pathlib import Path\n",
    "\n",
    "propagator = NumericalPropagator(state_init=state_init, gravity_file=Path(\"ICGEM_GOCO06s.gfc\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For configuring the degree and order of the gravity model, see the next section below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Gravitational model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following example configures gravity spherical harmonics with degree and order 64."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "propagator = NumericalPropagator(state_init=state_init, grav_degree_order=(64, 64))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following disables the spherical harmonics to only keep a Keplerian model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "propagator = NumericalPropagator(state_init=state_init, grav_degree_order=None)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Third-body perturbations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following adds the Sun as a third-body perturbation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ephemerista.bodies import Origin\n",
    "\n",
    "propagator = NumericalPropagator(state_init=state_init, third_bodies=[Origin(name=\"Sun\")])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following adds the Sun, the Moon and Jupiter as third-body perturbators."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "propagator = NumericalPropagator(\n",
    "    state_init=state_init, third_bodies=[Origin(name=\"Sun\"), Origin(name=\"luna\"), Origin(name=\"jupiter\")]\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Solar radiation pressure"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Simply use the `enable_srp` flag, which is `False` by default.\n",
    "\n",
    "Use `cross_section` to set the spacecraft cross-section in m^2 and `c_r` to set the reflection coefficient. [Reference](https://www.orekit.org/static/apidocs/org/orekit/forces/radiation/IsotropicRadiationSingleCoefficient.html)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "propagator = NumericalPropagator(state_init=state_init, enable_srp=True, cross_section=0.42, c_r=0.8)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Atmospheric drag"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Simply use the `enable_drag` flag, which is `False` by default.\n",
    "\n",
    "Use `cross_section` to set the spacecraft cross-section in m^2 and `c_d` to set the drag coefficient. [Reference](https://www.orekit.org/static/apidocs/org/orekit/forces/drag/IsotropicDrag.html)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "propagator = NumericalPropagator(state_init=state_init, enable_drag=True, cross_section=0.42, c_d=2.1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Full-fledged force model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "propagator = NumericalPropagator(\n",
    "    state_init=state_init,\n",
    "    mass=430.5,\n",
    "    cross_section=0.42,\n",
    "    c_d=2.1,\n",
    "    c_r=0.8,\n",
    "    grav_degree_order=(64, 64),\n",
    "    third_bodies=[Origin(name=\"Sun\"), Origin(name=\"luna\"), Origin(name=\"jupiter\")],\n",
    "    enable_srp=True,\n",
    "    enable_drag=True,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Propagator usage"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Propagating to a single date"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To perform a single propagation to a date:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "time_end = Time.from_iso(\"TDB\", \"2016-05-30T12:01:00\")\n",
    "\n",
    "state_end = propagator.propagate(time=time_end)\n",
    "display(state_end)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Propagating from a list of `Time`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To retrieve all the intermediary state vectors between the initial and the final states, pass a list of `Time` objects to the propagator's `propagate` method via the `time` argument:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "time_end = Time.from_iso(\"TDB\", \"2016-05-30T16:00:00\")\n",
    "t_step = 60.0  # s\n",
    "time_list = time_start.trange(time_end, t_step)\n",
    "\n",
    "trajectory = propagator.propagate(time=time_list)\n",
    "display(trajectory)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3D trajectory visualization"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This feature is actually available for other propagators such as `SGP4`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import plotly.graph_objects as go\n",
    "\n",
    "fig = go.Figure()\n",
    "\n",
    "fig.add_trace(trajectory.origin.plot_3d_surface())\n",
    "fig.add_trace(trajectory.plot_3d())\n",
    "\n",
    "fig.update_layout(\n",
    "    title=f\"3D trajectory, {trajectory.origin.name}-centered {trajectory.frame.abbreviation} frame\",\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For visualizing ground tracks, take a look at the `groundtracks.ipynb` notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Stopping conditions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is possible to stop propagation at events defined in the enum `ephemerista.propagators.events.StoppingEvent`. As of now, apoapsis and periapsis can be used as stopping conditions. The example below shows how to stop the propagation at periapsis. The same can be done for the apoapsis using the enum value `StoppingEvent.APOAPSIS` instead of `StoppingEvent.PERIAPSIS`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ephemerista.angles import Angle\n",
    "from ephemerista.coords.anomalies import TrueAnomaly\n",
    "from ephemerista.coords.shapes import RadiiShape\n",
    "from ephemerista.coords.twobody import Inclination, Keplerian\n",
    "from ephemerista.propagators.events import StoppingEvent\n",
    "from ephemerista.time import Time\n",
    "\n",
    "time_start = Time.from_iso(\"TDB\", \"2016-05-30T12:00:00\")\n",
    "\n",
    "state_init = Keplerian(\n",
    "    time=time_start,\n",
    "    shape=RadiiShape(ra=10000.0, rp=6800.0),  # km\n",
    "    inc=Inclination(degrees=98.0),\n",
    "    node=Angle(degrees=0.0),\n",
    "    arg=Angle(degrees=0.0),\n",
    "    anomaly=TrueAnomaly(degrees=90.0),\n",
    ")\n",
    "\n",
    "time_end = Time.from_iso(\"TDB\", \"2016-05-30T14:00:00\")\n",
    "t_step = 60.0  # s\n",
    "time_list = time_start.trange(time_end, t_step)\n",
    "\n",
    "propagator.set_initial_state(state_init)\n",
    "trajectory = propagator.propagate(time=time_list, stop_conds=[StoppingEvent.PERIAPSIS])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import plotly.graph_objects as go\n",
    "\n",
    "fig = go.Figure()\n",
    "\n",
    "fig.add_trace(trajectory.origin.plot_3d_surface())\n",
    "fig.add_trace(trajectory.plot_3d())\n",
    "\n",
    "fig.update_layout(\n",
    "    title=f\"3D trajectory, {trajectory.origin.name}-centered {trajectory.frame.abbreviation} frame\",\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# CCSDS OEM/OPM/OMM import/export"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`ephemerista` is able to read and write CCSDS OEM, OPM and OMM messages among others."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Writing CCSDS OEM files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`ephemerista` supports writing CCSDS files in both KVN and XML formats, relying on Orekit in the background."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ephemerista.propagators.orekit.ccsds import CcsdsFileFormat, write_oem\n",
    "\n",
    "dt = 300.0\n",
    "write_oem(trajectory, \"oem_example_out.txt\", dt, CcsdsFileFormat.KVN)\n",
    "write_oem(trajectory, \"oem_example_out.xml\", dt, CcsdsFileFormat.XML)\n",
    "\n",
    "with open(\"oem_example_out.txt\") as f:\n",
    "    display(f.readlines()[0:25])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reading CCSDS OEM files"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ephemerista.propagators.orekit.ccsds import parse_oem\n",
    "\n",
    "dt = 300.0\n",
    "trajectory = parse_oem(\"OEMExample5.txt\", dt)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import plotly.graph_objects as go\n",
    "\n",
    "fig = go.Figure()\n",
    "\n",
    "fig.add_trace(trajectory.origin.plot_3d_surface())\n",
    "fig.add_trace(trajectory.plot_3d())\n",
    "\n",
    "fig.update_layout(\n",
    "    title=f\"3D trajectory, {trajectory.origin.name}-centered {trajectory.frame.abbreviation} frame\",\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": ".venv",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
