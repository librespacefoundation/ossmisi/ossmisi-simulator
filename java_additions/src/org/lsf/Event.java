package org.lsf;

public enum Event {
    PERIAPSIS,
    APOAPSIS
}
