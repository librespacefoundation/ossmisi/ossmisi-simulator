import math
import typing
from typing import Literal, overload

import numpy as np
from numpy.typing import ArrayLike

BOLTZMANN_CONSTANT: float = 1.38064852e-23  # J/K
ROOM_TEMPERATURE = 290  # K
SPEED_OF_LIGHT_VACUUM: float = 2.99792458e8  # m/s


def wavelength(frequency: float) -> float:
    return SPEED_OF_LIGHT_VACUUM / frequency


@overload
def to_db(val: float) -> float: ...


@overload
def to_db(val: ArrayLike) -> ArrayLike: ...


def to_db(val: ArrayLike | float) -> ArrayLike | float:
    return 10 * np.log10(val)


@overload
def from_db(val: float) -> float: ...


@overload
def from_db(val: np.ndarray) -> np.ndarray: ...


def from_db(val: np.ndarray | float) -> np.ndarray | float:
    return 10 ** (val / 10)


def free_space_path_loss(distance: float, frequency: float) -> float:
    return to_db((4 * math.pi * distance * 1e3 / wavelength(frequency)) ** 2)


Band = Literal["HF", "VHF", "UHF", "L", "S", "C", "X", "Ku", "K", "Ka", "V", "W"]

BAND_NAMES = typing.get_args(Band)

BAND_FREQS = np.array([3e6, 30e6, 300e6, 1e9, 2e9, 4e9, 8e9, 12e9, 18e9, 27e9, 40e9, 75e9, 110e9])

MAX_FREQ = 110e9


def frequency_band(freq: float) -> Literal[Band] | None:
    if freq < BAND_FREQS[0] or freq > MAX_FREQ:
        return None
    return BAND_NAMES[np.flatnonzero(BAND_FREQS <= freq)[-1]]
