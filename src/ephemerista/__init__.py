import os
import platform
from pathlib import Path
from typing import Annotated, Literal

import lox_space as lox
import orekit_jpype
import pydantic
from pydantic import Field

from ephemerista.ipy_widgets import IpyWidgetHandler


class BaseModel(IpyWidgetHandler, pydantic.BaseModel):
    def __init__(self, **data):
        pydantic.BaseModel.__init__(self, **data)
        IpyWidgetHandler.__init__(self)


UT1Provider = lox.UT1Provider
Ephemeris = lox.SPK

EOP_PROVIDER: UT1Provider | None = None
EPHEMERIS: Ephemeris | None = None

JVM = {
    "Darwin": os.path.join("lib", "server", "libjvm.dylib"),
    "Linux": os.path.join("lib", "server", "libjvm.so"),
    "Windows": os.path.join("bin", "server", "jvm.dll"),
}[platform.system()]

JAR_FILE = "ephemeristaJava-0.0.1-SNAPSHOT.jar"


def init(
    *,
    eop_path: str | os.PathLike | None = None,
    spk_path: str | os.PathLike | None = None,
    jvm_path: str | os.PathLike | None = None,
    orekit_data: str | list[str] | os.PathLike | list[os.PathLike] | Literal["package"] = "package",
):
    if eop_path:
        global EOP_PROVIDER  # noqa: PLW0603
        EOP_PROVIDER = lox.UT1Provider(str(eop_path))

    if spk_path:
        global EPHEMERIS  # noqa: PLW0603
        EPHEMERIS = lox.SPK(str(spk_path))

    ephemerista_jar = Path(__file__).parent / "jars" / JAR_FILE

    if not ephemerista_jar.is_file():
        msg = f"{ephemerista_jar} not found"
        raise FileNotFoundError(msg)

    additional_classpaths = [str(ephemerista_jar)]

    if (jvm_path is None) and ("JAVA_HOME" not in os.environ):
        import jdk4py

        jvm_path = os.path.join(jdk4py.JAVA_HOME, JVM)

    orekit_jpype.initVM(additional_classpaths=additional_classpaths, jvmpath=jvm_path)

    filenames: str | list[str] | None = None
    from_pip_library = orekit_data == "package"

    if not from_pip_library:
        if isinstance(orekit_data, list):
            filenames = [str(f) for f in orekit_data]
        else:
            filenames = str(orekit_data)

    orekit_jpype.pyhelpers.setup_orekit_data(filenames=filenames, from_pip_library=from_pip_library)  # type: ignore


class MissingProviderError(Exception):
    pass


def get_eop_provider() -> UT1Provider | None:
    return EOP_PROVIDER


def eop_provider() -> UT1Provider:
    provider = get_eop_provider()
    if not provider:
        msg = "no EOP provider is available. Try calling `epehemerista.init`"
        raise MissingProviderError(msg)
    return provider


class MissingEphemerisError(Exception):
    pass


def get_ephemeris() -> Ephemeris | None:
    return EPHEMERIS


def ephemeris() -> Ephemeris:
    ephemeris = get_ephemeris()
    if not ephemeris:
        msg = "no ephemeris is available. Try calling `epehemerista.init`"
        raise MissingEphemerisError(msg)
    return ephemeris


def annotate_vec3_field(s):
    # Assumes there's only one vec3 per form
    s["$id"] = "/schemas/vec3"


type Vec3 = Annotated[
    tuple[float, float, float],
    Field(..., json_schema_extra=annotate_vec3_field),
]
