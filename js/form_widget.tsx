import * as React from "react";

import { createRender, useModel } from "@anywidget/react";

import Form, { IChangeEvent } from '@rjsf/core';
import validator from '@rjsf/validator-ajv8';
import { RJSFSchema, FieldProps, asNumber, RegistryFieldsType } from '@rjsf/utils';

import Loader from "react-spinners/PulseLoader";

import "./form_widget.css";
import 'jquery';
import "bootstrap/dist/css/bootstrap.min.css";

const Vec3Field = function (props: FieldProps) {

	const [state, setState] = React.useState({
		x: props.formData[0],
		y: props.formData[1],
		z: props.formData[2],
	});

	const onChange = function (component: string, event: React.ChangeEvent<HTMLInputElement>) {
		if (component == 'x') {
			setState({
				...state,
				x: event.target.value,
			});
		}

		if (component == 'y') {
			setState({
				...state,
				y: event.target.value,
			});
		}

		if (component == 'z') {
			setState({
				...state,
				z: event.target.value,
			});
		}

	};

	React.useEffect(() => {
		props.onChange([
			asNumber(state.x),
			asNumber(state.y),
			asNumber(state.z)
		]);
	}, [state]);

	return (
		<div>
			<label>
				{props.schema.title}
				{props.required ? '*' : null}
			</label>

			<p id={`${props.idSchema.$id}__description`} className="field-description">{props.schema.description}</p>

			<div className='form-row'>
				<div className='form-group col-sm-4'>
					<label htmlFor={`${props.idSchema.$id}__x`}>X</label>
					<input
						type='text'
						id={`${props.idSchema.$id}__x`}
						className='form-control'
						value={state.x}
						required={props.required}
						onChange={(event) => onChange('x', event)}
					/>
				</div>

				<div className='form-group col-sm-4'>
					<label htmlFor={`${props.idSchema.$id}__y`}>Y</label>
					<input
						type='text'
						id={`${props.idSchema.$id}__y`}
						className='form-control'
						value={state.y}
						required={props.required}
						onChange={(event) => onChange('y', event)}
					/>
				</div>

				<div className='form-group col-sm-4'>
					<label htmlFor={`${props.idSchema.$id}__z`}>Z</label>
					<input
						type='text'
						id={`${props.idSchema.$id}__z`}
						className='form-control'
						value={state.z}
						required={props.required}
						onChange={(event) => onChange('z', event)}

					/>
				</div>
			</div>
		</div>

	);
};

const render = createRender(() => {
	var loadingStage = 0;
	const [isLoading, setIsLoading] = React.useState(true);
	const [schema, setSchema] = React.useState<RJSFSchema>({});
	const [data, setData] = React.useState<Object>({});
	const [fields, setFields] = React.useState<RegistryFieldsType>({});

	function handleCustomMessage(customMessage: any) {
		if (customMessage["operation"] == "get_schema") {
			setSchema(customMessage["response"]);

			let fields: RegistryFieldsType = {};
			for (let element of customMessage["response"]["$vec3UniqueIds"]) {
				fields[element] = Vec3Field
			}
			setFields(fields);

			loadingStage += 1;
		}

		if (customMessage["operation"] == "get_initial_data") {
			setData(customMessage["response"]);
			loadingStage += 1;
		}

		if (loadingStage == 2) {
			setIsLoading(false);
		}
	}

	const model = useModel();

	React.useEffect(() => {
		model.on("msg:custom", handleCustomMessage);

		model.send({ "operation": "get_schema" });

		model.send({ "operation": "get_initial_data" });

		return () => model.off("msg:custom", handleCustomMessage);
	}, []);

	function onSubmit(data: IChangeEvent<any, any, any>, _event: React.FormEvent<any>) {
		for (var property in data.formData) {
			model.set(property, data.formData[property]);
		}

		model.save_changes();
	};

	if (isLoading) {
		return (
			<div className='ephemerista-form-widget'>
				<div className='spinner'>
					<Loader
						loading={true}
						size={10}
						aria-label='Loading Spinner'
						data-testid='loader'
					/>
				</div>
			</div>
		)
	}

	return (
		<div className='ephemerista-form-widget'>
			<Form
				schema={schema}
				formData={data}
				validator={validator}
				onSubmit={onSubmit}
				fields={fields}
				showErrorList='bottom'
			/>
		</div >
	);
});

export default { render };
