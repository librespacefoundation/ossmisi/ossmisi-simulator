# Examples

In-depth usage examples are provided as interactive Jupyter Notebooks in the [`examples` directory][examples] of the Ephemerista GitLab repository.

[examples]: https://gitlab.com/librespacefoundation/ephemerista/ephemerista-simulator/-/tree/main/examples
