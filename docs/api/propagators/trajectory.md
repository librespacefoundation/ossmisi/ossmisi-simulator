# Precomputed Trajectory Interpolator - `ephemerista.propagators.trajectory`

```{eval-rst}
.. automodule:: ephemerista.propagators.trajectory
    :members:
```
