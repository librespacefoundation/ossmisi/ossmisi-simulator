# Ephemerista

```{include} ../README.md
:start-after: <!-- start introduction -->
:end-before: <!-- end introduction -->
```

**Features**

```{include} ../README.md
:start-after: <!-- start features -->
:end-before: <!-- end features -->
```

```{toctree}
:hidden:
changelog
introduction/index
design/index
api/index
```
